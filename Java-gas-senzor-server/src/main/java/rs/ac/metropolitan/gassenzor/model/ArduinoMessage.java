/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.metropolitan.gassenzor.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;
import java.time.LocalDateTime;

/**
 *
 * @author Nemanja
 */
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class ArduinoMessage {

    @JsonProperty(value = "device")
    String device;

    @JsonProperty(value = "gas")
    String gas;

    @JsonProperty(value = "temperature")
    String temperature;

    @JsonProperty(value = "humidity")
    String humidity;
    
    @JsonProperty(value = "timestamp")
    LocalDateTime timstamp;

    public String getGas() {
        return gas;
    }

    public void setGas(String gas) {
        this.gas = gas;
    }

    public String getTemperature() {
        return temperature;
    }

    public void setTemperature(String temperature) {
        this.temperature = temperature;
    }

    public String getHumidity() {
        return humidity;
    }

    public void setHumidity(String humidity) {
        this.humidity = humidity;
    }

    public String getDevice() {
        return device;
    }

    public void setDevice(String device) {
        this.device = device;
    }

    public LocalDateTime getTimstamp() {
        return timstamp;
    }

    public void setTimstamp(LocalDateTime timstamp) {
        this.timstamp = timstamp;
    }
    
    
}
