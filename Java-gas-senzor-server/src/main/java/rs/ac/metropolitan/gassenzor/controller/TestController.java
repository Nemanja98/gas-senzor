/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.metropolitan.gassenzor.controller;



import java.sql.Timestamp;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import rs.ac.metropolitan.gassenzor.Util;
import rs.ac.metropolitan.gassenzor.db.ArduinoDao;
import rs.ac.metropolitan.gassenzor.model.ArduinoMessage;
import rs.ac.metropolitan.gassenzor.model.GraphElement;

/**
 *
 * @author Nemanja
 */
@Controller
public class TestController {
    
    @Autowired
    ArduinoDao dao;
    
    @GetMapping(value = "/position")
    public ResponseEntity<String> getPos(){
        String response = dao.state;
        return new ResponseEntity<>(response,HttpStatus.OK);         
    }
    
    //     @PostMapping(value = "/record")
    // public ResponseEntity<String> getInfo(@RequestParam String value){
        
    //     dao.state=value;
    //     return new ResponseEntity<>(HttpStatus.OK);         
    // }
    
   @PostMapping(value = "/record")
   public ResponseEntity<String> getInfo(@RequestBody ArduinoMessage value){
       System.out.println("GAS: " + value.getGas());
       System.out.println("HUMIDTY: " + value.getHumidity());
       System.out.println("TEMPERATURE: " + value.getTemperature());
       dao.storeData(value);
       return new ResponseEntity<>(HttpStatus.OK);         
   }
   
   @CrossOrigin
   @GetMapping(value = "/gas")
   public ResponseEntity<List<GraphElement>> getGas(
           @RequestParam String device,
           @RequestParam(required = false) String from,
           @RequestParam(required = false) String to) throws InterruptedException{
       
       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
       
       LocalDateTime fromDate = null;
       LocalDateTime toDate = null;
       
       if(from != null && to != null){
           fromDate = LocalDateTime.parse(from,formatter);
           toDate = LocalDateTime.parse(to,formatter);
       }
       List<GraphElement> response = dao.getData(fromDate, toDate, device).stream().map(m -> Util.mapGas(m)).collect(Collectors.toList());
        
       return new ResponseEntity<>(response,HttpStatus.OK);         
   }
   
   @CrossOrigin
   @GetMapping(value = "/humidity")
   public ResponseEntity<List<GraphElement>> getHumidity(
           @RequestParam String device,
           @RequestParam(required = false) String from,
           @RequestParam(required = false) String to) throws InterruptedException{
       
       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
       
               LocalDateTime fromDate = null;
       LocalDateTime toDate = null;
       
       if(from != null && to != null){
        fromDate = LocalDateTime.parse(from,formatter);
        toDate = LocalDateTime.parse(to,formatter);
       }
       List<GraphElement> response = dao.getData(fromDate, toDate, device).stream().map(m -> Util.mapHumidity(m)).collect(Collectors.toList());
        
       return new ResponseEntity<>(response,HttpStatus.OK);         
   }
   
   @CrossOrigin
   @GetMapping(value = "/temperature")
   public ResponseEntity<List<GraphElement>> getTemperature(
           @RequestParam String device,
           @RequestParam(required = false) String from,
           @RequestParam(required = false) String to) throws InterruptedException{
       
       DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
       
               LocalDateTime fromDate = null;
       LocalDateTime toDate = null;
       
       if(from != null && to != null){
        fromDate = LocalDateTime.parse(from,formatter);
        toDate = LocalDateTime.parse(to,formatter);
       }
       List<GraphElement> response = dao.getData(fromDate, toDate, device).stream().map(m -> Util.mapTemperature(m)).collect(Collectors.toList());
        
       return new ResponseEntity<>(response,HttpStatus.OK);         
   }
   
   @CrossOrigin
   @GetMapping(value = "/gas/peak")
   public ResponseEntity<List<GraphElement>> getGasPeaks(
           @RequestParam String device) throws InterruptedException{
       
       List<GraphElement> response = dao.getPeakData(device,"GAS").stream().map(m -> Util.mapGas(m)).collect(Collectors.toList());
        
       return new ResponseEntity<>(response,HttpStatus.OK);         
   }
   
   @CrossOrigin
   @GetMapping(value = "/humidity/peak")
   public ResponseEntity<List<GraphElement>> getHumidityPeaks(
           @RequestParam String device) throws InterruptedException{
       
       List<GraphElement> response = dao.getPeakData(device,"HUMIDITY").stream().map(m -> Util.mapHumidity(m)).collect(Collectors.toList());
        
       return new ResponseEntity<>(response,HttpStatus.OK);         
   }
   
   @CrossOrigin
   @GetMapping(value = "/temperature/peak")
   public ResponseEntity<List<GraphElement>> getTemperaturePeaks(
           @RequestParam String device) throws InterruptedException{
       
       List<GraphElement> response = dao.getPeakData(device,"TEMP").stream().map(m -> Util.mapTemperature(m)).collect(Collectors.toList());
        
       return new ResponseEntity<>(response,HttpStatus.OK);         
   }
    
}
