package rs.ac.metropolitan.gassenzor;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class GasSenzorApplication {

	public static void main(String[] args) {
		SpringApplication.run(GasSenzorApplication.class, args);
	}

}
