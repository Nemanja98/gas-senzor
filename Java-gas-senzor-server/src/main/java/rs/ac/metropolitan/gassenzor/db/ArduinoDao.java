/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.metropolitan.gassenzor.db;

import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.List;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.jdbc.core.JdbcTemplate;
import org.springframework.jdbc.core.PreparedStatementCallback;
import org.springframework.jdbc.core.RowMapper;
import org.springframework.jdbc.core.simple.SimpleJdbcCall;
import org.springframework.stereotype.Component;
import rs.ac.metropolitan.gassenzor.db.mapper.ArduinoDataMapper;
import rs.ac.metropolitan.gassenzor.model.ArduinoMessage;

/**
 *
 * @author Nemanja
 */
@Component
public class ArduinoDao {
    
  @Autowired
  JdbcTemplate jdbcTemplate;
  
  public String state = "default";
  
  private static final String INSERT_DATA = "INSERT INTO `senzor`(`DEVICE`, `GAS`, `HUMIDITY`, `TEMPERATURE`, `TIMESTAMP`)"
          + " VALUES (?,?,?,?,?)";
  private static final String SELECT_DATA = "SELECT * FROM `senzor` WHERE";
  
  private SimpleJdbcCall insertCall;
  
  
  public void storeData(ArduinoMessage message){
      
    jdbcTemplate.execute(INSERT_DATA,new PreparedStatementCallback<Boolean>(){  
    
    @Override  
    public Boolean doInPreparedStatement(PreparedStatement ps) throws SQLException, DataAccessException {  
              
        ps.setString(1,message.getDevice());  
        ps.setString(2,message.getGas());  
        ps.setString(3,message.getHumidity()); 
        ps.setString(4,message.getTemperature()); 
        ps.setString(5,LocalDateTime.now().toString()); 
              
        return ps.execute();  
              
    }  
    });
        
      
  }
  
  public List<ArduinoMessage> getData(LocalDateTime from, LocalDateTime to,String device){
    
    String sql = SELECT_DATA;
    
    sql+= "`DEVICE` = '" + device + "'";
    
    DateTimeFormatter formatter = DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss");
    
    if(from != null && to!=null){
        sql += " AND `TIMESTAMP` >= '" + from.format(formatter) + ""
                + "' AND `TIMESTAMP` <= '" + to.format(formatter) + "'";
         sql += " ORDER BY TIMESTAMP";
    }else{
        sql += " ORDER BY TIMESTAMP";
        sql += " LIMIT 10";
    }
    
   
      
      
    List<ArduinoMessage> data = jdbcTemplate.query(sql, new ArduinoDataMapper());
    
    return data;
      
  }
  
  public List<ArduinoMessage> getPeakData(String device, String type){
    
    String sql = SELECT_DATA;
    
    sql+= "`DEVICE` = '" + device + "'";
    
    
    switch(type){
        case "GAS": sql+= "AND GAS > 300 ";
                    break;
        case "TEMP": sql+= "AND TEMPERATURE > 35 ";
                    break; 
        case "HUMIDITY": sql+= "AND HUMIDITY > 80 ";
                    break;
    }
    
    sql += " ORDER BY TIMESTAMP";
    sql += " LIMIT 10";
      
    List<ArduinoMessage> data = jdbcTemplate.query(sql, new ArduinoDataMapper());
    
    return data;
      
  }
  
}
