#include "ESP8266WiFi.h"
#include "ESP8266HTTPClient.h"

const char* ssid = ""; //Enter SSID
const char* password = ""; //Enter Password

void setup(void)
{

  Serial.begin(115200);
  // Connect to WiFi
  WiFi.begin(ssid, password);
  while (WiFi.status() != WL_CONNECTED)
  {
    delay(500);
    Serial.print("*");
  }

  Serial.println("");
  Serial.println("WiFi connection Successful");
  Serial.print("The IP Address of ESP8266 Module is: ");
  Serial.print(WiFi.localIP());// Print the IP address
}

void loop()
{
  if (WiFi.status() == WL_CONNECTED) { //Check WiFi connection status
    HTTPClient http;    //Declare object of class HTTPClient
    http.begin("http://192.168.0.14:8080/position");      //Specify request destination
    http.addHeader("Content-Type", "application/json");  //Specify content-type header
    
    int httpCode = http.GET();   //Send the request
    String payload = http.getString();
    Serial.println(payload);

    http.end();  //Close connection
  } else {
    Serial.println("Error in WiFi connection");
  }
  delay(11000);  //Send a request every 11 seconds
}
