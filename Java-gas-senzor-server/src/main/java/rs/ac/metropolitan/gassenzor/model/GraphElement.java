/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.metropolitan.gassenzor.model;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.annotation.JsonProperty;
import com.fasterxml.jackson.databind.annotation.JsonSerialize;

/**
 *
 * @author Nemanja
 */
@JsonSerialize
@JsonInclude(JsonInclude.Include.NON_NULL)
public class GraphElement {

    @JsonProperty(value = "ts")
    String ts;

    @JsonProperty(value = "value")
    String value;

    public GraphElement(String ts, String value) {
        this.ts = ts;
        this.value = value;
    }
    
    

}
