/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.metropolitan.gassenzor.db.mapper;

import java.sql.ResultSet;
import java.sql.SQLException;
import org.springframework.jdbc.core.RowMapper;
import rs.ac.metropolitan.gassenzor.model.ArduinoMessage;

/**
 *
 * @author Nemanja
 */
public class ArduinoDataMapper implements RowMapper<ArduinoMessage> {

    @Override
    public ArduinoMessage mapRow(ResultSet rs, int i) throws SQLException {
        ArduinoMessage data = new ArduinoMessage();
 
        data.setGas(rs.getString("GAS"));
        data.setHumidity(rs.getString("HUMIDITY"));
        data.setTemperature(rs.getString("TEMPERATURE"));
        data.setTimstamp(rs.getTimestamp("TIMESTAMP").toLocalDateTime());
        
        return data;
        
    }
    
}
