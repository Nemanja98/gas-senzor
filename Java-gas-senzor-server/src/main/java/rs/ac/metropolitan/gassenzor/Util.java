/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package rs.ac.metropolitan.gassenzor;

import java.time.LocalDateTime;
import java.time.ZoneId;
import rs.ac.metropolitan.gassenzor.model.ArduinoMessage;
import rs.ac.metropolitan.gassenzor.model.GraphElement;

/**
 *
 * @author Nemanja
 */
public class Util {
    
    public static GraphElement mapGas(ArduinoMessage message){
        return new GraphElement(""+toEpochMilli(message.getTimstamp()),message.getGas());
    }
    
    public static GraphElement mapHumidity(ArduinoMessage message){
        return new GraphElement(""+toEpochMilli(message.getTimstamp()),message.getTemperature());
    }
    
    public  static GraphElement mapTemperature(ArduinoMessage message){
        return new GraphElement(""+toEpochMilli(message.getTimstamp()),message.getTemperature());
    }
    
    
    public static long toEpochMilli(LocalDateTime localDateTime)
    {
      return localDateTime.atZone(ZoneId.systemDefault())
        .toInstant().toEpochMilli();
    }
}
